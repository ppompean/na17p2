use vente

print("\nClean collection\n")
db.produit.deleteMany({})

/* le lien peut être déduit en réalité, juste pour montrer qu'on peut faire un peu de script */
print("\nAdd Dell Computer\n")
reference = "DellINSPIR40558"
db.produit.insert({
    reference:reference,
    nom:"Dell Inspirion 558",
    lien: "/produit/" + reference,
    description:"Une machine de guerre pour faire la guerre",
    type: "Appareil",
    extension:true,
    prix_de_reference:2500,
    marque:"Dell",
    consommation:{ valeur:5, unite:"Wh" },
    sous_categorie: "Gaming",
    categorie: "Ordinateurs"
})
inspirion = db.produit.find({ reference: reference })[0]._id

print("\nAdd Dell Fan\n")
reference = "Ventilox"
db.produit.insert({
    reference:reference,
    nom:"Ventilateur DELL",
    lien: "/produit/" + reference,
    description:"Ventilateur pour ordinateurs DELL",
    prix_de_reference:20,
    type: "Pièce Détachée",
    marque:"Dell",
    sous_categorie : "Bureau",
    categorie: "Ordinateurs",
    compatibilite: [ inspirion ]
})
ventilox = db.produit.find({ reference: reference })[0]._id

print("\nAdd Dell Fan to compatibilite array of Dell Computer\n")
// add ventilox to the compatibilite
db.produit.updateOne({ _id: inspirion }, { $push: { compatibilite: [ ventilox ] } })

print("\nAdd Huawei Phone\n")
reference = "Hua18"
db.produit.insert({
    reference:reference,
    nom:"Huawei 2018",
    extension:true,
    type: "Appareil",
    lien: "/produit/" + reference,
    description:"Trop super bien le lecteur d'empreintes, vous navez rien à cacher après tout",
    prix_de_reference:550,
    consommation:{ valeur:1000, unite:"mAh" },
    marque:"Huawei",
    sous_categorie : "Android",
    categorie: "Smartphones"
})

print("\nSmartphones less than 600€ from Huawei or Wiko\n")
db.produit.find({
    categorie: "Smartphones",
    prix_de_reference: { $lt: 600 },
    marque: { $in: [ "Huawei", "Wiko" ] }
})

print("\nSmartphones less than 600€ from Huawei or Wiko\n")
db.produit.find({
    categorie: "Smartphones",
    prix_de_reference: { $lt: 600 },
    marque: { $in: [ "Huawei", "Wiko" ] }
})

print("\nProducts compatible with the Ventilox\n")
compatible_ventilox = db.produit.find({ _id: ventilox })[0].compatibilite

db.produit.find({
  _id: { $in: compatible_ventilox }
})

