# Projet Personnel AI23

Paco Pompeani (ppompean)

Sujet: 32. Entreprise de vente

# Execute the SQL scripts

```shell
$ psql
psql > \i ./init.sql
psql > \i ./select.sql
```

# Execute the MongoDB shell script

```shell
mongo < ./init.mongoshell.js
```
