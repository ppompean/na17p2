# MLD

```
Produit (
	#reference : string
	nom : string
	description : string
	prix_de_reference : money
	consommation : entier
	unite_de_consommation : Wh, J, L/h
	extension_de_garantie_possible : boolean

	type : [ Appareil, Accessoire, PièceDetachee ]
	marque => Marque
	sous_categorie => SousCategorie
)
```
avec `#{nom, description, prix}`
avec `#{nom, description, sous_categorie}`
avec `#{nom, marque}`
l'absence d'attributs distincts des sous classes de Produit nous mène à les réunir en un type de la relation Produit.


```
Compatibilite_Produit (
	p1 => Produit
	p2 => Produit
)
```
avec `#{p1, p2}`

```
Marque (
	#nom : string
)
```

```
Categorie (
	#nom : string
)
```

```
Sous_Categorie (
	nom : string
	categorie => Categorie
)
```
avec `#{nom, categorie}`

```
Fournisseur (
	#nom : string
	#adresse : string
	#telephone : string
)
```
on retiendra nom comme clé primaire, beaucoup moins susceptible de changer.

```
Propose_Produit (
	produit => Produit
	fournisseur => Fournisseur
)
```
avec `#{produit, fournisseur}`

```
Article (
	#numero de serie : string
	prix_affiche : money
	extension_garantie : boolean
	produit => Produit
	fournisseur => Fournisseur
	vendu_sur => FactureVente
	utilisé_sur => FactureSAV
)
```
avec `not(FactureVente et FactureSAV)`
on ne retiendra qu'une clé pour cette relation.

```
Remise (
	pourcentage : 0 < entier < 100
	montant : money
	article => Article
)
```
avec `#{pourcentage, montant, article}`
la simplicité du test ci-dessous nous pousse à réunir les deux sous classes de Remise en une relation :
avec `pourcentage = 0 XOR montant = 0`

```
FactureVente (
	#reference_facture
	date : date
	installation_specialiste : boolean
	employe => Employe
	vendu_pour => Client
)
```
avec `employe.service = Vente`
on ne retiendra qu'une clé pour cette relation.

```
FactureSAV (
	date_demande : date
	date_reponse : date
	issue: [ irreparable, offre_de_reprise, reparation ]
	acceptation : boolean
	prix_reprise : money
	article => Article
	employe => Employe
	client => Client
)
```
avec `#{date_demande, article}`
avec `employe.service = SAV`
on laisse Client dans cette relation car l'Article peut changer de main avant de retourner en SAV.

```
Piece_Reparation (
	piece => Article
	sav => FactureSAV
)
```
avec `#{piece, sav}`

```
Client (
	#nom : string
	adresse : string
	#telephone : string
	professionnel : boolean
	nom_entreprise : string {optional}
)
```
on ne retiendra pas adresse comme clé pour les cas de colocations et autres formes de vie commune.
on retiendra nom comme clé primaire, beaucoup moins susceptible de changer.

```
Employe (
	nom : string
	arrivee : date
	depart : date (optional)
	service : [ Vente, Achat, SAV ]
)
```
avec `#{nom, service}` pour permettre à l'employé.e de changer de service (sa nouvelle représentation sera un nouveau tuple)
l'absence d'attributs distincts des sous classes de Employé nous mène à les réunir en un type de la relation Employé.

```
BonDeCommande (
	date_demande : date
	nombre_demande : entier > 0
	produit => Produit
	employe_sav => Employe
	date_reponse : date
	valide : boolean
	employe_achat => Employe
	date_achat : date
	prix_unitaire : money
	nombre_achetes : entier > 0
)
```
avec `#{date_demande, produit}`
on ne retiendra pas d'autre clé. Les informations à partir de date_reponse sont optionnelles et ajoutées au fur et à mesure de l'avancée du processus d'achat.
avec `employe_achat.service = achat`
avec `employe_sav.service = sav`
