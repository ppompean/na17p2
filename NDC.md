<!-- markdownlint-disable MD013 -->
# Note de clarification

## Questions

- Qu'est-ce que la consommation d'un produit ?
  - Réponse : Consommation énergétique
- Est-ce que le client paie les pièces détachées en cas de réparation ?
  - Réponse : Paiement si fin de garantie

## Classes

### Produit

- *peut etre compatible avec d'autres produits de manière symétrique*
- *possède une marque*
- nom
- référence
- prix de référence
- une sous-catégorie (qui appartient elle-même à une catégorie)
- type de produit : Appareil, Accessoire, Pièce détachée
- indicateur de possibilité d'étendre la garantie

### Marque

- nom

### Sous Catégorie

- *appartient à une catégorie*
- *les produits qui appartiennent à cette sous catégorie appartiennent à la catégorie par transitivité*
- nom

### Catégorie

- nom

### Fournisseur

- nom
- adresse
- téléphone

### Article

- *est un article unique qui représente un produit*
- *provient d'un fournisseur*
- *peut posséder plusieurs remises*
- numéro de série
- prix affiché
- indicateur d'extension de garantie (contraint par la possibilité d'extension du produit)

### FactureVente

- *possède un.e client.e*
- *peut calculer la somme à payer*
- *peut posséder une liste d'articles*
- *est effectuée par un.e employé.e du service Vente*
- date de facturation
- temps passé, si intervention SAV
- prix de l'installation (ce qui signifie que l'installation est à effectuer par un spécialiste)

### FactureSAV

- *possède un.e client.e*
- *peut calculer la somme à payer*
- *concerne un seul article*
- *est effectuée par un.e employé.e du service SAV*
- date de demande
- date de réponse
- issue de la demande : irréparable, offre de reprise, réparable
- acceptation (ou non) de la reprise ou de la réparation
- prix de la reprise en cas de reprise
- *peut posséder des pièces détachées en cas de réparation*

### Client

- nom
- type : particulier, professionnel

### Employé

- nom
- service : Vente, SAV, Réparation, Achat

### Bon de commande

- *porte sur un produit*
- *rédigé par un.e employé.e du service Vente*
- *répondu et acheté par un.e employé.e du service Achat*
- date de demande
- nombre demandé
- date de réponse
- demande validée
- date d'achat
- nombre acheté
- prix unitaire négocié

