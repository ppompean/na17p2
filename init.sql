-- Contraintes d'intégrité non gérées par la BDD :

-- Les pièces achetées pour réparation doivent être des pièces détachées.
--                                                  compatibles avec le Produit réparé.
--                                                  doivent êtres liées à une facture SAV dont l'issue a en effet été la réparation.

-- Les Articles doivent provenir d'un Fournisseur qui propose le Produit auquel ils sont liés.

-- Le Client qui amène un produit au SAV n'est pas forcément le Client qui l'a acheté. Bug or Feature?

-- L'ensemble des remises d'un produit peuvent excéder le prix du produit.

-- Questionnements :

-- Peut-être qu'il vaudrait mieux que les employés existent dans 3 tables différentes en fonction de leur service. On peut imaginer que les si les employés étient gérés de manière plus complète (en incluant toute la partie RH), il vaudrait mieux qu'ils soient tous dans la même table.
-- -> On pourrait faire une liste datée des services dans lesquels un.e employé.e a travaillé.
-- Peut-être qu'il vaudrait mieux séparer le bon de commande en 3 tables (en relation 1-1) selon l'état de la demande plutôt que d'introduire des checks complexes.

DROP SCHEMA IF EXISTS vente CASCADE;
CREATE SCHEMA vente;

CREATE TABLE vente.marque (
	nom VARCHAR PRIMARY KEY
);
INSERT INTO vente.marque VALUES
('Dell'), ('Acer'), ('HP'), ('Huawei'), ('Apple'), ('Raspberry Pi'), ('Arduino'), ('MadameMonsieur');

CREATE TABLE vente.categorie (
	nom VARCHAR PRIMARY KEY
);
INSERT INTO vente.categorie VALUES
('Ordinateurs'), ('Smartphones'), ('Embarqués'), ('Électroménager');

CREATE TABLE vente.sous_categorie (
	nom VARCHAR,
	categorie VARCHAR NOT NULL REFERENCES vente.categorie(nom),
	PRIMARY KEY(nom, categorie)
);
INSERT INTO vente.sous_categorie VALUES
('Gaming', 'Ordinateurs'),
('Laptops', 'Ordinateurs'),
('Bureau', 'Ordinateurs'),
('Android', 'Smartphones'),
('Apple', 'Smartphones'),
('Raspberry Pi', 'Embarqués'),
('Arduino', 'Embarqués'),
('Petit Électroménager', 'Électroménager');

CREATE TABLE vente.produit (
	reference VARCHAR PRIMARY KEY,
	nom VARCHAR NOT NULL,
	description TEXT NOT NULL,
	prix_de_reference NUMERIC(8,2) NOT NULL CHECK (prix_de_reference > 0),
	consommation INTEGER CHECK (consommation >= 0),
	unite_de_consommation VARCHAR CHECK (unite_de_consommation IN ('mAh', 'Wh', 'J', 'L/h')),
	extension_de_garantie_possible BOOLEAN NOT NULL,
	type VARCHAR NOT NULL CHECK (type IN ('Appareil', 'Accessoire', 'PieceDetachee')),
	marque VARCHAR NOT NULL REFERENCES vente.marque(nom),
	sous_categorie VARCHAR NOT NULL,
	categorie VARCHAR NOT NULL,
	FOREIGN KEY(sous_categorie, categorie) REFERENCES vente.sous_categorie(nom, categorie),
        UNIQUE(nom, description, prix_de_reference),
        UNIQUE(nom, description, sous_categorie),
        UNIQUE(nom, marque)
);
INSERT INTO vente.produit VALUES
('Hua18', 'Huawei 2018', 'Trop super bien le lecteur dempreintes, vous navez rien à cacher après tout', 550, 1000, 'mAh', TRUE, 'Appareil', 'Huawei', 'Android', 'Smartphones'),
('Hua18CoqueRED', 'Coque rouge pour Huawei 2018', 'Super trop beau le rouge', 4, NULL, NULL, FALSE, 'Accessoire', 'Huawei', 'Android', 'Smartphones'),
('Hua18CoqueBLUE', 'Coque bleue pour Huawei 2018', 'Super trop beau le bleu', 4, NULL, NULL, FALSE, 'Accessoire', 'Huawei', 'Android', 'Smartphones'),
('DellINSPIR40558', 'Dell Inspirion 558', 'Une machine de guerre pour faire la guerre', 2500, 5, 'Wh', TRUE, 'Appareil', 'Dell', 'Gaming','Ordinateurs'),
('Ventilox', 'Ventilateur DELL', 'Ventilateur pour ordinateurs DELL', 20, NULL, NULL, FALSE, 'PieceDetachee', 'Dell', 'Bureau', 'Ordinateurs'),
('028LMSTEVPK', 'Steve le Jouet', 'Certifié par les allergologues', 40, NULL, NULL, TRUE, 'Appareil', 'MadameMonsieur', 'Petit Électroménager', 'Électroménager');

CREATE TABLE vente.compatibilite_produit (
	p1 VARCHAR REFERENCES vente.produit(reference),
	p2 VARCHAR REFERENCES vente.produit(reference),
	PRIMARY KEY (p1, p2)
);
INSERT INTO vente.compatibilite_produit VALUES
('Ventilox', 'DellINSPIR40558'),
('Hua18', 'Hua18CoqueRED'),
('Hua18', 'Hua18CoqueBLUE');

CREATE TABLE vente.fournisseur (
	nom VARCHAR PRIMARY KEY,
	adresse VARCHAR UNIQUE NOT NULL,
	telephone VARCHAR UNIQUE NOT NULL CHECK (telephone ~ '^\+?[0-9]{6,15}$')
);
INSERT INTO vente.fournisseur VALUES
('Fourni2000', '2000 allée des lièvres 93100 Montroix', '0605040302'),
('Kanban', '3 rue des magnolias 94300 Vinçoix', '0605040301'),
('FNAC', '60 avenue W. Churchill 60200 Compoix', '0148484848'),
('Obsesion', '69 avenue du Progrès 60200 Compoix', '0609060906');

CREATE TABLE vente.propose_produit (
	produit VARCHAR REFERENCES vente.produit(reference),
	fournisseur VARCHAR REFERENCES vente.fournisseur(nom),
	PRIMARY KEY(produit, fournisseur)
);
INSERT INTO vente.propose_produit VALUES
('Hua18', 'Fourni2000'),
('Hua18CoqueRED', 'Fourni2000'),
('Hua18CoqueBLUE', 'Fourni2000'),
('DellINSPIR40558', 'Kanban'),
('DellINSPIR40558', 'FNAC'),
('Ventilox', 'FNAC'),
('028LMSTEVPK', 'Obsesion');

CREATE TABLE vente.client (
	nom VARCHAR PRIMARY KEY,
	adresse VARCHAR UNIQUE NOT NULL,
	telephone VARCHAR UNIQUE NOT NULL CHECK (telephone ~ '^\+?[0-9]{6,15}$'),
	professionnel BOOLEAN NOT NULL CHECK (professionnel = TRUE OR professionnel = FALSE), -- kinda genius, I know
	nom_entreprise VARCHAR,
	CHECK ((professionnel = TRUE AND nom_entreprise IS NOT NULL) OR (professionnel = FALSE AND nom_entreprise IS NULL))
);
INSERT INTO vente.client VALUES
('John Doe', '1 avenue de la République, 60200 Compoix', '118212', FALSE, NULL),
('Jorette Placisse', '68 rue de Mai 1968, 60400 Vanoix', '0631346600', TRUE, '4Xvues'),
('Irma Daosta', 'Isollaz, Challand Saint-Victor', '0781236664', FALSE, NULL);

CREATE TABLE vente.employe (
	nom VARCHAR,
	arrivee DATE NOT NULL,
	depart DATE,
	service VARCHAR CHECK (service IN ('Vente', 'Achat', 'SAV')),
	PRIMARY KEY (nom, service)
);
INSERT INTO vente.employe VALUES
('Lucia Kopp', '2018-06-28', NULL, 'Achat'),
('Paul Pin', '1998-03-15', NULL, 'SAV'),
('Polo Beltramo', '2019-05-21', NULL, 'SAV'),
('Pierre Manoure', '2007-01-02', '2007-03-03', 'Vente'),
('Tanina Panini', '2007-03-04', '2008-03-03', 'Vente'),
('Léa Savicki', '2005-10-08', NULL, 'Vente'),
('Léon Bevan', '2015-08-26', NULL, 'Vente');

CREATE TABLE vente.facture_vente (
	reference_facture INTEGER GENERATED BY DEFAULT AS IDENTITY (START WITH 10) PRIMARY KEY,
	date DATE NOT NULL,
	installation_specialiste BOOLEAN NOT NULL,
	employe_nom VARCHAR NOT NULL,
	employe_service VARCHAR NOT NULL CHECK (employe_service = 'Vente'),
	FOREIGN KEY(employe_nom, employe_service) REFERENCES vente.employe(nom, service),
	client VARCHAR NOT NULL REFERENCES vente.client(nom)
);
INSERT INTO vente.facture_vente VALUES
(1, '2007-02-18', FALSE, 'Pierre Manoure', 'Vente', 'Jorette Placisse'),
(2, '2016-08-24', FALSE, 'Léon Bevan', 'Vente', 'John Doe'),
(3, '2019-11-30', TRUE,  'Léa Savicki', 'Vente', 'Irma Daosta'),
(4, '2019-05-29', FALSE, 'Léon Bevan', 'Vente', 'John Doe');

CREATE TABLE vente.facture_sav (
	date_demande DATE,
	date_reponse DATE,
	issue VARCHAR CHECK (issue IN ('irreparable', 'offre_de_reprise', 'reparation')),
	acceptation BOOLEAN,
	prix_reprise NUMERIC(8,2) CHECK (prix_reprise > 0),
	article VARCHAR,
	employe_nom VARCHAR NOT NULL,
	employe_service VARCHAR NOT NULL CHECK (employe_service = 'SAV'),
	FOREIGN KEY(employe_nom, employe_service) REFERENCES vente.employe(nom, service),
	client VARCHAR NOT NULL REFERENCES vente.client(nom),
	PRIMARY KEY (date_demande, article),
        UNIQUE(date_demande, article),
	CHECK (date_demande <= date_reponse)
);
INSERT INTO vente.facture_sav VALUES
('2018-05-17', '2018-05-23', 'reparation', TRUE, NULL, 'DellINSPIR40558AAAAAAAADDDDA', 'Paul Pin', 'SAV', 'Jorette Placisse'),
('2020-05-27', '2020-05-28', 'offre_de_reprise', TRUE, 30, 'Stev000001', 'Polo Beltramo', 'SAV', 'Irma Daosta');

CREATE TABLE vente.article (
	numero_serie VARCHAR PRIMARY KEY,
	prix_affiche NUMERIC(8,2) NOT NULL CHECK (prix_affiche > 0),
	extension_garantie BOOLEAN NOT NULL, --TODO check if boolean can be true ?
	produit VARCHAR NOT NULL REFERENCES vente.produit(reference),
	fournisseur VARCHAR NOT NULL REFERENCES vente.fournisseur(nom),
	vendu_sur INTEGER REFERENCES vente.facture_vente(reference_facture),
	utilise_pour_sav_date DATE,
	utilise_pour_sav_article VARCHAR,
	FOREIGN KEY (utilise_pour_sav_date, utilise_pour_sav_article) REFERENCES vente.facture_sav(date_demande, article) MATCH FULL,
	-- check that article can't be in 2 factures
	CHECK (vendu_sur IS NULL OR utilise_pour_sav_date IS NULL)
);
INSERT INTO vente.article VALUES
('Hua180000001', 559, TRUE, 'Hua18', 'Fourni2000', NULL, NULL, NULL);
INSERT INTO vente.article VALUES
('Hua180000003', 559, TRUE, 'Hua18', 'Fourni2000', 2, NULL, NULL);
INSERT INTO vente.article VALUES
('Hua18CoqueRED00000001', 6, TRUE, 'Hua18CoqueRED', 'Fourni2000', 2, NULL, NULL),
('DellINSPIR40558AAAAAAAAAAAAA', 2599, FALSE, 'DellINSPIR40558', 'FNAC', NULL, NULL, NULL),
('DellINSPIR40558AAAAAAAAAAAAB', 2599, FALSE, 'DellINSPIR40558', 'FNAC', 1, NULL, NULL),
('DellINSPIR40558AAAAAAAAAAAAC', 2599, FALSE, 'DellINSPIR40558', 'FNAC', 4, NULL, NULL),
('DellINSPIR40558AAAAAAAADDDDA', 2599, FALSE, 'DellINSPIR40558', 'Kanban', 1, NULL, NULL),
('Ventilox0000001', 20, FALSE, 'Ventilox', 'FNAC', NULL, '2018-05-17', 'DellINSPIR40558AAAAAAAADDDDA'),
('Ventilox0000002', 20, FALSE, 'Ventilox', 'FNAC', NULL, NULL, NULL),
('Ventilox0000003', 20, FALSE, 'Ventilox', 'FNAC', NULL, NULL, NULL),
('Ventilox0000004', 20, FALSE, 'Ventilox', 'FNAC', NULL, NULL, NULL),
('Stev000001', 39, TRUE, '028LMSTEVPK', 'Obsesion', 3, NULL, NULL),
('Stev000002', 39, TRUE, '028LMSTEVPK', 'Obsesion', NULL, NULL, NULL);

-- We could not do that without the article table and the article table needed vente.facture_sav too.
ALTER TABLE vente.facture_sav
ADD CONSTRAINT facture_sav_article_fkey FOREIGN KEY (article) REFERENCES vente.article(numero_serie) MATCH FULL;

CREATE TABLE vente.remise (
	pourcentage INTEGER CHECK (0 <= pourcentage AND pourcentage < 100),
	montant NUMERIC(8,2) NOT NULL CHECK (montant >= 0),
	article VARCHAR REFERENCES vente.article(numero_serie),
	CHECK (pourcentage = 0 AND montant != 0 OR pourcentage != 0 AND montant = 0),
	PRIMARY KEY (pourcentage, montant, article)
);
INSERT INTO vente.remise VALUES
(0, 1, 'Hua18CoqueRED00000001'),
(5, 0, 'DellINSPIR40558AAAAAAAADDDDA'),
(15, 0, 'DellINSPIR40558AAAAAAAAAAAAC'),
(0, 11, 'DellINSPIR40558AAAAAAAAAAAAC');

/*
C'est une relation 0..1:1..n : elle n'a pas besoin d'une table dédiée.

CREATE TABLE vente.piece_reparation (
	article VARCHAR REFERENCES vente.article(numero_serie),
	date_sav DATE,
	article_sav VARCHAR,
	FOREIGN KEY (date_sav, article_sav) REFERENCES vente.facture_sav(date_demande, article),
	PRIMARY KEY (article, date_sav, article_sav)
);
INSERT INTO vente.piece_reparation VALUES
('Ventilox0000001', '2018-05-17', 'DellINSPIR40558AAAAAAAADDDDA');
*/

CREATE TABLE vente.bon_de_commande (
	-- first step
	date_demande DATE,
	nombre_demande INTEGER NOT NULL CHECK (nombre_demande > 0),
	produit VARCHAR REFERENCES vente.produit(reference),
	sav_nom VARCHAR NOT NULL,
	sav_service VARCHAR NOT NULL CHECK (sav_service = 'SAV'),
	FOREIGN KEY (sav_nom, sav_service) REFERENCES vente.employe(nom, service),
	PRIMARY KEY (date_demande, produit),
	-- second step
	date_reponse DATE,
	valide BOOLEAN,
	achat_nom VARCHAR,
	achat_service VARCHAR CHECK (achat_service = 'Achat'),
	FOREIGN KEY (achat_nom, achat_service) REFERENCES vente.employe(nom, service) MATCH FULL,
	-- third step
	date_achat DATE,
	prix_unitaire NUMERIC(8,2) CHECK (prix_unitaire > 0),
	nombre_achete INTEGER CHECK (nombre_achete > 0),
	-- check for dates integrity
	CHECK (date_reponse BETWEEN date_demande AND date_achat),
	-- check for completeness of second step
	CHECK ((date_reponse IS NULL AND valide IS NULL AND achat_nom IS NULL) OR (date_reponse IS NOT NULL AND valide IS NOT NULL AND achat_nom IS NOT NULL)),
	-- check for completeness of third step
	CHECK ((date_achat IS NULL AND prix_unitaire IS NULL AND nombre_achete IS NULL) OR (date_achat IS NOT NULL AND prix_unitaire IS NOT NULL AND nombre_achete IS NOT NULL)),
	-- check that third step can be here only if the second step is already here
	CHECK (date_achat IS NULL OR date_reponse IS NOT NULL)
);
INSERT INTO vente.bon_de_commande VALUES
-- refused
('2017-08-11', 5, 'Hua18', 'Paul Pin', 'SAV', '2017-08-11', FALSE, 'Lucia Kopp', 'Achat', NULL, NULL, NULL),
-- accepted and bought
('2017-09-07', 5, 'Hua18', 'Paul Pin', 'SAV', '2017-09-10', TRUE, 'Lucia Kopp', 'Achat', '2017-09-25', 315.55, 2),
-- accepted but not yet bought
('2020-04-27', 2, 'Hua18CoqueBLUE', 'Polo Beltramo', 'SAV', '2020-04-28', TRUE, 'Lucia Kopp', 'Achat', NULL, NULL, NULL),
-- not yet answered
('2020-05-29', 10, '028LMSTEVPK', 'Polo Beltramo', 'SAV', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

