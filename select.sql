-- SQL SELECT

-- Liste des Produits vendus par Fournisseur
SELECT f.nom fournisseur,
p.nom produit,
COUNT(a.numero_serie) vendus,
MAX(v.date) dernière_vente
FROM vente.article a
INNER JOIN vente.produit p ON a.produit = p.reference
INNER JOIN vente.fournisseur f ON a.fournisseur = f.nom
INNER JOIN vente.facture_vente v ON a.vendu_sur = v.reference_facture
GROUP BY p.nom, f.nom
ORDER BY vendus DESC, dernière_vente DESC;
-- WHERE f.nom = 'Fourni2000';

-- Liste des Produits les plus écoulés
SELECT produit, utilises, en_stock,
CASE WHEN en_stock = 0 THEN 0 ELSE (en_stock * 100) / (utilises + en_stock) END pourcentage_stock
FROM (
    SELECT p.nom produit,
        COUNT(
        CASE WHEN a.vendu_sur IS NOT NULL OR a.utilise_pour_sav_date IS NOT NULL THEN 1 ELSE NULL END
        ) utilises,
        COUNT(
        CASE WHEN a.numero_serie IS NOT NULL AND a.vendu_sur IS NULL AND a.utilise_pour_sav_date IS NULL THEN 1 ELSE NULL END
        ) en_stock
    FROM vente.produit p
    LEFT JOIN vente.article a ON a.produit = p.reference
    GROUP BY p.nom
) subquery
ORDER BY pourcentage_stock ASC, en_stock ASC;

-- Liste les Produits vendus et leur taux de retour en SAV
SELECT produit,
articles_vendus,
articles_retournés,
(articles_retournés * 100) / (articles_vendus) taux_de_retour
FROM (
    SELECT
    p.nom produit,
    COUNT(a.numero_serie) articles_vendus,
    COUNT(f.date_demande) articles_retournés
    FROM vente.article a
    INNER JOIN vente.produit p ON a.produit = p.reference
    LEFT JOIN vente.facture_sav f ON a.numero_serie = f.article
    WHERE a.vendu_sur IS NOT NULL
    GROUP BY p.nom
) subquery
ORDER BY taux_de_retour DESC;

-- Affiche la somme des remises effectuées pour chaque article vendu, accompagnées de leur employé, sur les 4 dernières années
SELECT employé,
date_d_arrivée,
date,
facture,
article,
prix_original,
to_char(SUM(argent_perdu), '999999D99') argent_perdu
FROM (
    SELECT e.nom employé,
    e.arrivee date_d_arrivée,
    f.date date,
    f.reference_facture facture,
    a.numero_serie article,
    a.prix_affiche prix_original,
    CASE WHEN r.montant = 0 THEN (a.prix_affiche * r.pourcentage) / 100 ELSE r.montant END argent_perdu
    FROM vente.employe e
    INNER JOIN vente.facture_vente f ON e.nom = f.employe_nom AND e.service = f.employe_service
    INNER JOIN vente.article a ON f.reference_facture = a.vendu_sur
    INNER JOIN vente.remise r ON r.article = a.numero_serie
    WHERE e.service = 'Vente'
    AND e.depart IS NULL
    AND age(f.date) <= '4 years'::interval
) subquery
GROUP BY employé, date_d_arrivée, date, facture, article, prix_original
ORDER BY date_d_arrivée DESC, employé, date DESC;

-- Qui achète le plus ? Particuliers ou Professionnels ? Pour chaque catégorie de produits.
SELECT
-- Faire la somme des prix de vente, pour chaque catégorie de produit et de client
CASE c.professionnel WHEN TRUE THEN 'Professionnels' ELSE 'Particuliers' END type_clients,
p.categorie,
to_char(SUM(v.prix_vendu), '999999D99') prix_total
FROM (
    -- Calculer les prix de vente réels de chaque produit
    SELECT
    r.numero_serie,
    MAX(r.prix_original) - SUM(r.remise) prix_vendu
    FROM (
        -- Calculer les remises de chaque produit
        SELECT
        a.numero_serie,
        CASE WHEN r.montant IS NULL THEN
            0
        ELSE
            CASE WHEN r.montant = 0 THEN (a.prix_affiche * r.pourcentage) / 100 ELSE r.montant END
        END remise,
        a.prix_affiche prix_original
        FROM vente.article a
        LEFT JOIN vente.remise r ON r.article = a.numero_serie
        WHERE vendu_sur IS NOT NULL
    ) r
    GROUP BY r.numero_serie
) v
INNER JOIN vente.article a ON a.numero_serie = v.numero_serie
INNER JOIN vente.facture_vente f ON a.vendu_sur = f.reference_facture
INNER JOIN vente.client c ON f.client = c.nom
INNER JOIN vente.produit p ON p.reference = a.produit
GROUP BY c.professionnel, p.categorie
ORDER BY p.categorie, prix_total DESC;
